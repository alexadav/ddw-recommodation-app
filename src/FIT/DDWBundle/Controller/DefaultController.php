<?php

namespace FIT\DDWBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	public function indexAction()
	{
		if ($this->getRequest()->getMethod() == 'POST') {
			$idu = $this->getRequest()->get('idu');
			$amount = $this->getRequest()->get('amount');
			return $this->redirect($this->generateUrl('detail', array('idu' => $idu, 'amount' => $amount)));
		}

		$retArr = array();

		$this->assignIduArr($retArr);

		return $this->render('FITDDWBundle:Default:index.html.twig', $retArr);
	}

	public function detailAction($idu, $amount = 10)
	{
		$retArr = array();
		$client = $this->get('codag.predictionio')->getClient();

		$this->assignIduArr($retArr);

		try {
			// assume you have created an itemrec engine named 'rec'
			// we try to get top 10 recommendations for a user (user ID 5)
			$client->identify($idu);
			$command = $client->getCommand('itemrec_get_top_n', array('pio_engine' => 'rec', 'pio_n' => $amount));
			$rec = $client->execute($command);
			$retArr['recIdvs'] = $this->getImageData($rec['pio_iids']);

			$this->setVisitedAndBoughtIdvs($idu, $retArr);
		} catch (Exception $e) {
			echo 'Caught exception: ', $e->getMessage(), "\n";
		}

		$retArr['userId'] = $idu;

		return $this->render('FITDDWBundle:Default:detail.html.twig', $retArr);
	}

	public function importAction()
	{
		$client = $this->get('codag.predictionio')->getClient();
		$newUserID = 200000;

		$conn = $this->get('database_connection');

		// import all products
		$sql = 'SELECT idv FROM product_variant';
		foreach ($conn->query($sql) as $row) {
			$command = $client->getCommand('create_item', array('pio_iid' => $row['idv'], 'pio_itypes' => 'poster'));
			$response = $client->execute($command);
		}

		// import all users
		$sql = 'SELECT idu FROM user_history WHERE idu != 0';
		foreach ($conn->query($sql) as $row) {
			$command = $client->getCommand('create_user', array('pio_uid' => $row['idu']));
			$response = $client->execute($command);
		}

		// assign all bought products and visited products for user
		$sql = 'SELECT idu, content
			FROM user_history';
		foreach ($conn->query($sql) as $row) {
			$idu = $row['idu'];
			if ($idu == 0) {
				$idu = $newUserID++;
			}
			$content = (array)unserialize($row['content']);

			$client->identify($idu);

			if (isset($content['cart'])) {
				$cart = (array)$content['cart'];
				$cartValues = $cart['value'];

				foreach ($cartValues as $item) {
					$client->execute($client->getCommand('record_action_on_item', array('pio_action' => 'conversion', 'pio_iid' => $item['idv'])));
				}
			}

			if (isset($content['visitedIdv'])) {
				$idvs = (array)$content['visitedIdv'];
				$idvsValues = $idvs['value'];

				foreach ($idvsValues as $key => $prod) {
					$client->execute($client->getCommand('record_action_on_item', array('pio_action' => 'view', 'pio_iid' => $prod)));
				}
			}
		}

		$this->getRequest()->getSession()->getFlashBag()->add('success', 'All data were imported');

		return $this->redirect($this->generateUrl('homepage'));
	}

	private function assignIduArr(&$retArr) {
		$conn = $this->get('database_connection');
		$sql = 'SELECT idu FROM user_history WHERE idu != 0';

		$iduArr = array();
		foreach ($conn->query($sql) as $row) {
			array_push($iduArr, $row['idu']);
		}
		asort($iduArr);
		$retArr['iduArr'] = $iduArr;

		$idu = $this->getRequest()->get('idu');
		if ($idu) {
			$retArr['idu'] = $idu;
		}
		$amount = $this->getRequest()->get('amount');
		if ($amount) {
			$retArr['amount'] = $amount;
		}
	}

	private function getImageData($inputIdvs) {
		$idvs = array();

		if (!count($inputIdvs)) {
			return array();
		}

		$conn = $this->get('database_connection');
		$sql = 'SELECT i.idi, p.idv, i.idp
			FROM image2product i
			JOIN product_variant p ON p.idp = i.idp
			WHERE p.idv IN ('.implode(",", $inputIdvs).')';

		$iduArr = array();
		foreach ($conn->query($sql) as $row) {
			$key = array_search($row['idv'], $inputIdvs);
			if ($key !==  false) {
				$idvs[$row['idv']] = $row['idi'];
			}
		}

		return $idvs;
	}

	private function setVisitedAndBoughtIdvs($idu, &$retArr) {
		$conn = $this->get('database_connection');
		$viewIdvs = $cartIdvs = array();

		// get all visited and bought items
		$sql = 'SELECT idu, content
			FROM user_history
			WHERE idu != 0';
		foreach ($conn->query($sql) as $row) {
			$locIdu = $row['idu'];
			if ($locIdu != $idu) {
				continue;
			}
			$content = (array)unserialize($row['content']);

			if (isset($content['cart'])) {
				$cart = (array)$content['cart'];
				$cartValues = $cart['value'];

				foreach ($cartValues as $item) {
					array_push($cartIdvs, $item['idv']);
				}
			}

			if (isset($content['visitedIdv'])) {
				$idvs = (array)$content['visitedIdv'];
				$idvsValues = $idvs['value'];

				foreach ($idvsValues as $key => $idv) {
					array_push($viewIdvs, $idv);
				}
			}
		}
		$retArr['viewIdvs'] = $this->getImageData($viewIdvs);
		$retArr['cartIdvs'] = $this->getImageData($cartIdvs);
	}
}
